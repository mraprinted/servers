Elastic APM Pyhton Django
==========================


Install the APM agent
---------------------

.. code-block:: bash

    $ pip install elastic-apm

or add it to your project’s ``requirements.txt`` file.

Setup
-----
1. Add variables in uwsgi.ini [#]_ files.

**Production**

.. code-block:: bash

    APM_NAME: Bhisma-Datindonesia #Change me
    APM_NAME_FE: Bhisma-Datindonesia-Frontend
    APM_URL: https://apm.gramedia.info:8200
    APM_PASS: Jr3CjnKBWdqJrS
    NAME_FROM_ROUTE: True


**Developemnt or Staging**

.. code-block:: bash

    #APM Service 
    APM_NAME: Bhisma-Datindonesia #Change me
    APM_NAME_FE: Bhisma-Datindonesia-Frontend
    APM_URL: http://localhost:8200
    APM_PASS: xxxxxx
    NAME_FROM_ROUTE: True

2. Add *elasticapm.contrib.django* to *INSTALLED_APPS* in your ``settings.py``:

.. code-block:: python

    INSTALLED_APPS = (
    # APM Service
    'elasticapm.contrib.django',
    )

3. Choose a service name, and set the secret token.

.. code-block:: python

    #Elastic APM settings
    ELASTIC_APM = {
      'SERVICE_NAME': env.string('APM_NAME', 'django-app'),
      'SERVER_URL' : env.string('APM_URL', ''),
      'DEBUG': True,
      'SECRET_TOKEN': env.string('APM_PASS', ''),
      'DJANGO_TRANSACTION_NAME_FROM_ROUTE': env.boolean('NAME_FROM_ROUTE', True),
    }

Performance Metrics
--------------------
To send performance metrics to Elastic APM, simply add our tracing middleware to your *MIDDLEWARE* settings:

.. code-block:: python

    MIDDLEWARE = [
      #Elastic APM middleware
      'elasticapm.contrib.django.middleware.TracingMiddleware',
      ...
    ]


Logging & Loggers
-----------------
For fine-grained control over logging use Python’s built-in logging module. If you are new to how the logging module works together with Django

.. code-block:: python

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            #Elastic APM Loging
            'elasticapm': {
            'level': 'WARNING',
            'class': 'elasticapm.contrib.django.handlers.LoggingHandler',
        },
            #For APM loggers
            'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
      'loggers': {
        'Datindonesia': { #Change me
            'handlers': ['app', 'mail_admins', 'elasticapm'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'oscar.core': {
            'handlers': ['app', 'mail_admins', 'elasticapm'],
            'level': 'DEBUG',
            'propagate': True,
        },
        # Log errors from the Elastic APM module to the console (recommended)
        'elasticapm.errors': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
         },
      },
  }

Integrating with the rum agent
------------------------------
To correlate performance measurement in the browser with measurements in your Django app, you can help the RUM (Real User Monitoring) agent by configuring it with the Trace ID and Span ID of the backend request. We provide a handy template context processor which adds all the necessary bits into the context of your templates.

To enable this feature, first add the rum_tracing context processor to your TEMPLATES setting. You most likely already have a list of context_processors, in which case you can simply append ours to the list.

.. code-block:: python

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'OPTIONS': {
               'context_processors': [
                    # ...
                    'elasticapm.contrib.django.context_processors.rum_tracing',
                ],
            },
        },
    ]

Then, update the call to initialize the RUM agent (which probably happens in your base.html template) like this:

.. code-block:: html

    <script src='{% static "js/elastic-apm-rum.umd.min.js" %}' crossorigin></script>
    <script>
    $(function () {
    elasticApm.init({
        serviceName: "{{ APM_NAME_FE }}",
        serverUrl: "{{ APM_URL }}",
        pageLoadTraceId: "{{ apm.trace_id }}",
        pageLoadSpanId: "{{ apm.span_id }}",
        pageLoadSampled: {{ apm.is_sampled_js }},
        distributedTracingOrigins: ['*']
    });
    });
    </script>

Download ``elastic-apm-rum-umd.min.js`` in `github <https://github.com/elastic/apm-agent-rum-js/releases>`_.

See the `JavaScript RUM agent documentation <https://www.elastic.co/guide/en/apm/agent/rum-js/current>`_ for more information.

Add variables in ``context_processors.py`` for frontned

.. code-block:: bash

    #APM Variable Frontend
    APM_URL = env.string('APM_URL', '')
    APM_NAME_FE = env.string('APM_NAME_FE', '')


Reference
----------

See more details about documentations

* |apm_django|.
* |apm_rumjs|.

Example Changes
---------------

See this commit

* |commit_change|.

* |context_change|.

* |template_change|.

* |setting_change|.

.. Footnote
.. [#] Usually the variable part set by *devops*.

.. Define link for new tab

.. |commit_change| raw:: html

    <a href="https://dev.azure.com/gramediadigital/Bisma/_git/MOVED-datindonesia/commit/3a6abbebd54910c8878586a0e3cde37b66d16367?path=%2Fsrc%2Fdatindonesia%2Fdatindonesia%2Fsettings.py&gridItemType=2&mpath=%2Fsrc%2Fdatindonesia%2Fdatindonesia%2Fsettings.py&opath=%2Fsrc%2Fdatindonesia%2Fdatindonesia%2Fsettings.py&mversion=GC3a6abbebd54910c8878586a0e3cde37b66d16367&oversion=GC26aa5d7cbb08560e0c0d917b57267c02fff743a0&_a=compare" target="_blank">Bhisma Datindonesia Changes</a>

.. |context_change| raw:: html

    <a href="https://dev.azure.com/gramediadigital/Bisma/_git/MOVED-datindonesia/commit/3a6abbebd54910c8878586a0e3cde37b66d16367?path=%2Fsrc%2Fdatindonesia%2Fdatindonesia%2Fcontext_processors.py&gridItemType=2&mpath=%2Fsrc%2Fdatindonesia%2Fdatindonesia%2Fcontext_processors.py&opath=%2Fsrc%2Fdatindonesia%2Fdatindonesia%2Fcontext_processors.py&mversion=GC3a6abbebd54910c8878586a0e3cde37b66d16367&oversion=GC26aa5d7cbb08560e0c0d917b57267c02fff743a0&_a=compare" target="_blank">Bhisma Datindonesia Context_processor</a>

.. |template_change| raw:: html

    <a href="https://dev.azure.com/gramediadigital/Bisma/_git/MOVED-datindonesia/commit/3a6abbebd54910c8878586a0e3cde37b66d16367?path=%2Fsrc%2Fdatindonesia%2Ftemplates%2Fbase.html&gridItemType=2&mpath=%2Fsrc%2Fdatindonesia%2Ftemplates%2Fbase.html&opath=%2Fsrc%2Fdatindonesia%2Ftemplates%2Fbase.html&mversion=GC3a6abbebd54910c8878586a0e3cde37b66d16367&oversion=GC26aa5d7cbb08560e0c0d917b57267c02fff743a0&_a=compare" target="_blank">Bhisma Datindonesia Template Base</a>

.. |setting_change| raw:: html

    <a href="https://dev.azure.com/gramediadigital/Bisma/_git/MOVED-datindonesia?path=%2Fsrc%2Fdatindonesia%2Fdatindonesia%2Fsettings.py&version=GBmaster" target="_blank">Bhisma Datindonesia Full Settings</a>

.. |apm_django| raw:: html

   <a href="https://www.elastic.co/guide/en/apm/agent/python/current/django-support.html" target="_blank">Elastic Apm Django</a>

.. |apm_rumjs| raw:: html

    <a href="https://www.elastic.co/guide/en/apm/server/6.7/rum.html" target="_blank">Elastic Apm RumJs</a>