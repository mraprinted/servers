Elastic APM Uwsgi Variables
============================

Setup Variables
----------------

1. Add variables in **Ansible** [#]_.

**Production**

.. code-block:: bash

    #APM Service 
    apm_name: "{{ app_full_name }}" #Example : Bhisma-Datindonesia
    apm_name_fe: "{{ app_full_name }}-Frontend"
    apm_url: "https://apm.gramedia.info:8200"
    apm_pass: "Jr3CjnKBWdqJrS"
    apm_name_from_route: "True"

**Developemnt or Staging**

.. code-block:: bash

    #APM Service 
    apm_name: "{{ app_full_name }}" #Example : Bhisma-Datindonesia
    apm_name_fe: "{{ app_full_name }}-Frontend"
    apm_url: "http://localhost:8200"
    apm_pass: "xxxxxx"
    apm_name_from_route: "True"


2. Call value variables in uwsgi.ini file.

.. code-block:: bash

    APM_NAME: "{{ apm_name }}"
    APM_NAME_FE: "{{ apm_name_fe }}"
    APM_URL: "{{ apm_url }}"
    APM_PASS: "{{ apm_pass }}"
    NAME_FROM_ROUTE: "{{ apm_name_from_route }}"

variable apm_pass [#]_

Reference
----------

See more details about documentations

* |apm_django|.
* |apm_rumjs|.

Example Changes
---------------
See this commit

* |vars_change|.

.. Footnote
.. [#] Put variables in Ansible Playbook files. 
.. [#] apm_pass value must with encrypted strings.

.. Define link for new tab
.. |vars_change| raw:: html

   <a href="https://dev.azure.com/gramediadigital/Bisma/_git/MOVED-datindonesia/commit/31251608f586c26a5cb34b00d61295ba7a7ab446?path=%2Fdeploy%2Fenvironments%2Fproduction%2Fgroup_vars%2Fdat-omnibus%2Fenv_vars&gridItemType=2&mpath=%2Fdeploy%2Fenvironments%2Fproduction%2Fgroup_vars%2Fdat-omnibus%2Fenv_vars&opath=%2Fdeploy%2Fenvironments%2Fproduction%2Fgroup_vars%2Fdat-omnibus%2Fenv_vars&mversion=GC31251608f586c26a5cb34b00d61295ba7a7ab446&oversion=GCf6c4add577e8753b778151d6bfcc48fc59012880&_a=compare" target="_blank">Bhisma Datindonesia variable changes</a>


.. |apm_django| raw:: html

   <a href="https://www.elastic.co/guide/en/apm/agent/python/current/django-support.html" target="_blank">Elastic Apm Django</a>

.. |apm_rumjs| raw:: html

    <a href="https://www.elastic.co/guide/en/apm/server/6.7/rum.html" target="_blank">Elastic Apm RumJs</a>