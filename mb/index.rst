Application Performance Monitoring
==================================
Application Performance Monitoring (APM) collects in-depth performance metrics and errors from inside your application. It allows you to monitor the performance of thousands of applications in real time.

**Conventions**

- Apm Backend Phyton Django.
- Apm Frontend JavaScript Agent.


**Table of Contents**

.. toctree::
    :maxdepth: 1

    guide/index
    devops/index
