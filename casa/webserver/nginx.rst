Nginx
=====

Service nginx digunakan sebagai webserver yang langsung di akses oleh user, nginx di pilih karena memakan resource yang tidak terlalu boros dan dukungan module yang banyak , seperti http2,brotli,pagespeed dan lainnya, konfigurasi yang digunakan pada casa menggunakan proxy_pass yang di mana nanti, nginx akan meneruskan permintaan ke webserver apache, karena pada nginx tidak dapat mengolah bahasa pemograman *Php*, maka digunakan apache sebagai pengolah bahasa pemograman *Php*. Terdapat alternatif lain yaitu menggunkan *Php-fpm*, namun php-fpm lebih ke konfigurasi, kurang begitu dinamik dan perlu penyesuaian setiap perubahan spek server atau perubahan traffik user, sehingga dipilih adalah apache, yang pada dasarnya sudah lebih dinamis dan dapat menyesuaikan secara otomatis ketika ada perubahan traffik user atau spek server.

Struktur folder
----------------

* /etc/nginx

Pada folder ini terdapat 1 file config bernama ``nginx.conf``.
File tersebut digunakan untuk merubah konfigurasi pada nginx itu sendiri, Pengaktifan module seperti brotli, gzip, cache dan juga pengaktifkan module tambahan lainnya, juga di sini, pada file ini terdapat banyak perubahan yg di sesuaikan untuk casa.

Berikut ini adalah full file konfigurasinya

.. code-block:: conf

    user  nginx;
    worker_processes  auto;
    pid        /var/run/nginx.pid;

    ##
    # Extra modules
    ##
        include /etc/nginx/modules.conf; #<- extra modules

    events {
        worker_connections  1024;
    }

    http {
        include       /etc/nginx/mime.types;
        include       /etc/nginx/win-utf;
        include       /etc/nginx/koi-utf;
        include       /etc/nginx/koi-win;
        default_type  application/octet-stream;
        server_tokens off;

    ##
    # cache static img
    ##
       proxy_cache_valid   any     10m;
       proxy_cache_path /var/cache/nginx/img levels=1:2 keys_zone=img_cache:10m max_size=1G;
       proxy_temp_path  /var/cache/nginx/tmp;
       proxy_buffer_size 4k;
       proxy_buffers 100 8k;
       proxy_connect_timeout       90;
       proxy_send_timeout          90;
       proxy_read_timeout          90;

        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';
    ##
    # Logs
    ##

    #     error_log  /dev/null warn; #<- disable log
         access_log  /dev/null  main; #<- disable log

        error_log  /var/log/nginx/error.log warn; #<- Enable
    #    access_log  /var/log/nginx/access.log  main; #<- Enable

        sendfile            on;
        tcp_nopush          on;
        tcp_nodelay         on;
        keepalive_timeout   65;
        types_hash_max_size 2048;

    ##
    # BROTLI
    ##

    brotli on;
    brotli_static on;
    brotli_buffers 16 8k; #32 8k
    brotli_comp_level 7;
    brotli_types *;

    ##
    # GZIP
    ##
            gzip on;
            gzip_disable "MSIE [1-6]\.";

            gzip_vary on;
            gzip_static on;
            gzip_proxied any;
            gzip_comp_level 7;
            gzip_buffers 16 8k;
            gzip_http_version 1.1;
            gzip_min_length 256;
            gzip_types 
            text/plain 
            text/css 
            application/json 
            application/x-javascript 
            text/xml 
            application/xml 
            application/xml+rss 
            text/javascript 
            application/javascript
            application/x-font-opentype 
            application/x-font-truetype 
            application/x-font-ttf 
            font/eot 
            font/opentype 
            font/otf
                    image/png 
                    image/gif 
                    image/jpeg
                    image/webp
                    image/jpg;
        include /etc/nginx/conf.d/*.conf;
    }

* /etc/nginx/conf.d

Pada folder ini digunakan untuk konfigurasi virtual host, yang ingin kita gunakan, di sini saya menambahkan 4 file, file tersebut digunakan untuk membuat virtual host pada server staging dan juga server produksi, dan juga ada konfigutasi untuk module pagespeed serta domain co.id

**Virtual server Coid.conf**
File ini digunakan untuk domain .co.id

.. code-block:: bash

    server {
    charset utf-8;
        add_header X-Cache-Status $upstream_cache_status;
        add_header Access-Control-Allow-Origin *;
        listen 80;
        server_tokens off;

       proxy_hide_header X-Powered-By;
       proxy_hide_header server;
       server_name casaindonesia.co.id;
       root /var/www/html/casa/coid;

    #.git patch

        location ~ /\.ht {
            deny all;
            access_log off;
            log_not_found off;
        }

        location ~ / {
                proxy_pass http://127.0.0.1:8080;

              proxy_set_header    Host             $host;
              proxy_set_header    X-Real-IP        $remote_addr;
              proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
              proxy_set_header    X-Client-Verify  SUCCESS;
              proxy_set_header    X-Client-DN   $ssl_client_s_dn;
              proxy_set_header    X-SSL-Subject    $ssl_client_s_dn;
              proxy_set_header    X-SSL-Issuer     $ssl_client_i_dn;
              proxy_set_header X-Forwarded-Proto https;
              add_header X-Cache-Status $upstream_cache_status;
              client_max_body_size 100M;
             client_body_buffer_size 1m;
             proxy_intercept_errors on;
             proxy_buffering on;
             proxy_buffer_size 128k;
             proxy_buffers 256 16k;
             proxy_busy_buffers_size 256k;
             proxy_temp_file_write_size 256k;
             proxy_max_temp_file_size 0;
             proxy_read_timeout 300;
        }
        }

**Virtual server staging.conf**
File ini digunakan untuk virtual server staging

.. code-block:: bash

    server {
    charset utf-8;
        add_header X-Cache-Status $upstream_cache_status;
        add_header Access-Control-Allow-Origin *;
        listen 80;
        server_tokens off;
       proxy_hide_header X-Powered-By;
       proxy_hide_header server;
       server_name staging.casaindonesia.com;
       root /var/www/html/casa/casaindonesia;

    #.git patch

        location ~ /\.ht {
            deny all;
            access_log off;
            log_not_found off;
        }

    ##
    # Resize & Crop
    # Digunakan untuk rezise atau crop photo dengan module nginx image_filter
    ##
        location ~* ^/lkgallery/teaser/resize/([\d\-]+)x([\d\-]+)/(.+)$ {
                alias /var/www/html/casa/staging/lkgallery/teaser/$3;
                image_filter resize $1 $2;
                image_filter_jpeg_quality 72;
                image_filter_buffer 2M;
                image_filter_interlace on;
        }

        location ~* ^/lkgallery/teaser/crop/([\d\-]+)x([\d\-]+)/(.+)$ {
            alias /var/www/html/casa/staging/lkgallery/teaser/$3;
            image_filter crop $1 $2;
            image_filter_jpeg_quality 72;
            image_filter_buffer 2M;
            error_page 415 = /empty;
        }

        location ~ / {
                proxy_pass http://127.0.0.1:8080;

              proxy_set_header    Host             $host;
              proxy_set_header    X-Real-IP        $remote_addr;
              proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
              proxy_set_header    X-Client-Verify  SUCCESS;
              proxy_set_header    X-Client-DN   $ssl_client_s_dn;
              proxy_set_header    X-SSL-Subject    $ssl_client_s_dn;
              proxy_set_header    X-SSL-Issuer     $ssl_client_i_dn;
              proxy_set_header X-Forwarded-Proto https;
              add_header X-Cache-Status $upstream_cache_status;
             client_max_body_size 100M;
             client_body_buffer_size 1m;
             proxy_intercept_errors on;
             proxy_buffering on;
             proxy_buffer_size 128k;
             proxy_buffers 256 16k;
             proxy_busy_buffers_size 256k;
             proxy_temp_file_write_size 256k;
             proxy_max_temp_file_size 0;
             proxy_read_timeout 300;
        }
        }

**Virtual server default.conf**
Untuk file ini digunakan untuk konfigurasi production

.. code-block:: bash

    server {
    charset utf-8;
        add_header X-Cache-Status $upstream_cache_status;
        add_header Access-Control-Allow-Origin *;
        listen 80;
        server_tokens off;

       proxy_hide_header X-Powered-By;
       proxy_hide_header server;
       server_name casaindonesia.com www.casaindonesia.com;
       root /var/www/html/casa/htdocs;

    #.git patch

        location ~ /\.ht {
            deny all;
            access_log off;
            log_not_found off;
        }

    ##
    # Resize & Crop
    # Digunakan untuk rezise atau crop photo dengan module nginx image_filter
    ##
        location ~* ^/lkgallery/teaser/resize/([\d\-]+)x([\d\-]+)/(.+)$ {
                alias /var/www/html/casa/htdocs/lkgallery/teaser/$3;
                image_filter resize $1 $2;
                image_filter_jpeg_quality 95;
                image_filter_buffer 2M;
                image_filter_interlace on;
        }

        location ~* ^/lkgallery/teaser/crop/([\d\-]+)x([\d\-]+)/(.+)$ {
            alias /var/www/html/casa/htdocs/lkgallery/teaser/$3;
            image_filter crop $1 $2;
            image_filter_jpeg_quality 95;
            image_filter_buffer 2M;
            error_page 415 = /empty;
        }

        location ~ / {
                proxy_pass http://127.0.0.1:8080;

              proxy_set_header    Host             $host;
              proxy_set_header    X-Real-IP        $remote_addr;
              proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
              proxy_set_header    X-Client-Verify  SUCCESS;
              proxy_set_header    X-Client-DN   $ssl_client_s_dn;
              proxy_set_header    X-SSL-Subject    $ssl_client_s_dn;
              proxy_set_header    X-SSL-Issuer     $ssl_client_i_dn;
              proxy_set_header X-Forwarded-Proto https;
              add_header X-Cache-Status $upstream_cache_status;
             client_max_body_size 100M;
             client_body_buffer_size 1m;
             proxy_intercept_errors on;
             proxy_buffering on;
             proxy_buffer_size 128k;
             proxy_buffers 256 16k;
             proxy_busy_buffers_size 256k;
             proxy_temp_file_write_size 256k;
             proxy_max_temp_file_size 0;
             proxy_read_timeout 300;
        }
        }

**Virtual server pagespeed.conf**
Untuk file ini digunakan untuk konfigurasi module pagespeed, file ini dapat mengaktifkan pagespeed dan juga menonaktifkan pagespeed, file ini bersifat global, karena berdiri pada satu file sendiri, jika di aktifkan maka domain production dan staging ikut aktif

.. code-block:: bash

    ##
    # Author : R Farid Nugraha
    # Email : < farid_@msn.com >
    # For : MRA Media
    ##

    ##
    # Global Seting
    ##
    pagespeed on;
    pagespeed XHeaderValue "Powered By MRA Media";
    pagespeed MessageBufferSize 102400;
    pagespeed FileCachePath "/var/cache/pagespeed";
    pagespeed EnableCachePurge on;
    pagespeed PurgeMethod PURGE;
    pagespeed LogDir /var/log/nginx/pagespeed;
    pagespeed AddResourceHeader "Access-Control-Allow-Origin" "*";
    pagespeed FetchHttps enable;
    pagespeed CssFlattenMaxBytes 5120;
    pagespeed GoogleFontCssInlineMaxBytes 5120;
    pagespeed CssInlineMaxBytes 5120;
    pagespeed AllowVaryOn auto;
    pagespeed EnableFilters jpeg_subsampling;
    pagespeed EnableFilters make_show_ads_async;
    pagespeed EnableFilters make_google_analytics_async;
    #pagespeed EnableFilters local_storage_cache;
    pagespeed ForceCaching on;


    ##
    # filters
    ##
    pagespeed RewriteLevel CoreFilters; #<- default config 
    #pagespeed RewriteLevel PassThrough; #<- Advance User
    pagespeed EnableFilters collapse_whitespace;
    pagespeed EnableFilters remove_comments;    
    pagespeed EnableFilters remove_quotes;
    pagespeed EnableFilters convert_meta_tags;
    pagespeed EnableFilters insert_dns_prefetch;
    pagespeed EnableFilters combine_heads;

    ##
    # css
    ##
    pagespeed EnableFilters inline_css;
    pagespeed EnableFilters rewrite_css;
    pagespeed EnableFilters move_css_to_head;
    pagespeed EnableFilters prioritize_critical_css;
    pagespeed EnableFilters combine_css;
    pagespeed EnableFilters inline_google_font_css;
    pagespeed EnableFilters flatten_css_imports;
    #pagespeed EnableFilters move_css_above_scripts;

    ##
    # js
    ##
    pagespeed EnableFilters inline_javascript;
    pagespeed EnableFilters defer_javascript;
    pagespeed EnableFilters rewrite_javascript;
    pagespeed EnableFilters canonicalize_javascript_libraries;
    pagespeed EnableFilters combine_javascript;
    pagespeed EnableFilters elide_attributes;
    pagespeed EnableFilters extend_cache;

    ##
    # statistic
    ##
    #pagespeed Statistics off;
    #pagespeed StatisticsLogging on;
    #pagespeed StatisticsLoggingIntervalMS 60000;
    #pagespeed StatisticsLoggingMaxFileSizeKb 51200;
    #pagespeed UsePerVhostStatistics on;
    #pagespeed StatisticsPath /ngx_pagespeed_statistics;
    #pagespeed GlobalStatisticsPath /ngx_pagespeed_statistics;
    #pagespeed MessagesPath /ngx_pagespeed_statistics;
    #pagespeed ConsolePath /ngx_pagespeed_statistics;
    #pagespeed AdminPath /ngx_pagespeed_statistics;
    #pagespeed GlobalAdminPath /ngx_pagespeed_statistics;

Jika ingin menambahkan virtualhost, hanya perlu salin dari salah satu file, default atau file staging, dan ganti nama domiannya.