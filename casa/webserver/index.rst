Webserver
=========

Pada halaman ini terdapat dua webserver yang digunakan, *Apache* dan juga *Nginx*.
Nginx digunakan untuk menerima semua permintaan dari klien atau pengguna, 
teknologi yang digunakan pada nginx adalah *HTTP2*, *PageSpeed*, *Brotli*, *Resizer*.
Apache digunakan untuk mengolah bahasa pemograman *Php*, karena pada *nginx* tidak menggunakan *php-fpm*.

.. toctree::
    :maxdepth: 1

    apache
    nginx
