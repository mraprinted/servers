NFS Client
===========
Nfs client digunakan untuk mounting asset fot dan video dari server nfs, semua foto yang di simpan pada server casa nanti akan ada di server nfs tersebut, dan dapat di share atau di gunakan oleh server lain, fungsi dari nfs itu sendiri , hampir sama seperti samba pada windows, satu jaringan dapat mengambil atau memodifikasi file tersebut, pada mra server nfs ada pada ip ``10.130.64.23``

Struktur folder
----------------

* /etc/fstab

agar asset pada nfs server dapat di akses pada saat server restart atau reboot, maka di perlukan perubahan pada file ``/etc/fstab``, perubahan tersbut untuk menambahkan ip nfs server dan lokasi tujuan mounting file pada server client

Berikut ini isi dari konfigurasi di file tersebut

.. code-block:: bash

    10.130.64.23:/mnt/volume_sgp1_01/nfsshare/casa/lkgallery /var/www/html/casa/htdocs/lkgallery  nfs defaults 0 0
    10.130.64.23:/mnt/volume_sgp1_01/nfsshare/casa/staging/lkgallery /var/www/html/casa/staging/lkgallery  nfs defaults 0 0

``10.130.64.23`` adalah ip nfs server.

``:/mnt/volume_sgp1_01/nfsshare/casa/lkgallery`` adalah posisi atau letak file yang ingin di share pada server.

``/var/www/html/casa/htdocs/lkgallery`` adalah posisi atau letak file yang dituju pada server client.

``nfs`` adalah jenis partisi, disni menggunakan jenis partisi nfs.

untuk aktifkan ketik 

``mount -a``