FROM python:3.6

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

#To create pdf file install this
#RUN apt-get update && apt-get install -y texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended latexmk

COPY . .

CMD make html

#To build pdf file
CMD make latexpdf

