NFS (Network FileSystem)
=========================
Nfs client digunakan untuk mounting asset fot dan video dari server nfs, semua foto yang di simpan pada server casa nanti akan ada di server nfs tersebut, dan dapat di share atau di gunakan oleh server lain, fungsi dari nfs itu sendiri , hampir sama seperti samba pada windows, satu jaringan dapat mengambil atau memodifikasi file tersebut, pada mra server nfs ada pada ip ``10.130.64.23``

.. toctree::
    :maxdepth: 1

    nfs/index
