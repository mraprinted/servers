### DevOps Docs: Technical Guide For Developer

===============================================

Repository ini dibuat dengan tujuan untuk memberikan dokumentasi teknikal dengan kolaborasi pada developer

Cara menggunakan
----------------

```bash
git clone git@ssh.dev.azure.com:v3/gramediadigital/Operations/devops-docs; cd devops-docs
```

Kemudian install

```bash
pip install requirements.txt
```
atau

```bash
pip3 install requirements.txt
```
disarankan menggunakan python3

Membuat html
------------

untuk membuat html gunakan perintah berikut
```bash
make html
```

Membuat file pdf
----------------

untuk membuat pdf di perlukan modul tambahan

```bash
sudo apt update -y && sudo apt install -y  texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended latexmk
```
dan gunakan perintah berikut untuk membuat file pdf
```bash
make latexpdf
```